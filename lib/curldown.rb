require 'curb'
require 'json'
require 'redcarpet'
require 'nokogiri'
require 'pygments.rb'
module Curldown
  def self.render(user: false, repo: false, url: false, github: false, bitbucket: false)
    instance = Render.new(user: user, repo: repo, url: url, github: github, bitbucket: bitbucket)
    instance.render
    instance.highlight!
    instance
  end
  def get(url, json: true)
    http = Curl::Easy.new(url)
    http.perform
    if http.status[0] == "2"
      if json
        response = JSON.parse(http.body_str)
      else
        response = http.body_str
      end
    else
      raise StandardError.new("get method returned #{http.status} code when calling #{http.url}")
    end
    response
  end
  class Render
    attr_accessor :user, :repo, :url, :github, :bitbucket, :raw, :html, :lexers
    def initialize(user: false, repo: false, url: false, github: false, bitbucket: false)
      @lexers = []
      Pygments.lexers.each{|k,v| @lexers << v[:aliases] }
      @lexers.flatten!
      if url
        @raw = get(url, json: false)
      else
        if github
          req = GitHub.new(user, repo)
        elsif bitbucket
          req = BitBucket.new(user, repo)
        else
          raise StandardError.new("Must specify either: github, bitbucket or url parameters.")
        end
        req.perform
        @raw= req.readme_md
      end
    end
    def render
      md = Redcarpet::Markdown.new(Redcarpet::Render::HTML, fenced_code_blocks: true)
      @html= md.render(@raw)
    end
    def highlight!
      doc = Nokogiri::HTML(@html)
      doc.css("pre").each do |pre|
        attrs = pre.children[0].attributes['class']
        if attrs
          lang = validate_lexer(attrs.value)
        else
          lang = "sh"
        end
        highlight = Pygments.highlight(pre.content, :lexer => lang)
        parsed = Nokogiri::HTML(highlight).css(".highlight")
        pre.add_next_sibling(parsed)
        pre.remove
      end
      @html = doc.css("body").children.to_html
    end
    def validate_lexer(string)
      @lexers.include?(string) ? string : "sh"
    end

  end
  class GitHub
    include Curldown
    CORE_URL = "https://raw.githubusercontent.com"
    attr_accessor :user, :repo_name, :readme_md
    def initialize(u, r)
      @user= u
      @repo_name= r
    end
    def perform
      get_readme
    end
    def get_readme
      ["readme.md", "README.md", "README.MD", "Readme.md"].each{|file|
        begin
          @readme_md= get("#{CORE_URL}/#{@user}/#{@repo_name}/master/#{file}", json: false)
        rescue
          false
        end
      }
      if !@readme_md
        raise StandardError.new("No readme file found.")
      else
        @readme_md
      end
    end

  end
  class BitBucket
    include Curldown
    CORE_URL = "https://api.bitbucket.org/2.0"
    attr_accessor :user, :repo_name, :commit, :tree, :readme_file, :repo_object, :readme_md
    def initialize(u, r)
      @user= u
      @repo_name= r
    end
    def perform
      get_repo
      get_last_commit
      get_tree_object
      get_readme_file
      get_readme_md
    end
    def get_repo
      @repo_object= get("#{CORE_URL}/repositories/#{@user}/#{@repo_name}")
      @repo_name= repo_object['name']
      @repo_slug= repo_object['slug']
    end
    def get_last_commit
      commits_url = repo_object['links']['commits']['href']
      commits = get(commits_url)
      @commit= commits['values'][0]['hash']
    end
    def get_tree_object
      @tree= get("#{CORE_URL}/repositories/#{@user}/#{@repo_slug}/src/#{@commit}/")
    end
    def get_readme_file
      tree['values'].each{|v|
        if v['path'].match?(/readme.md/i)
          @readme_file= v['path']
        end
      }
      if !readme_file
        raise RuntimeError.new("No readme file in repository")
      end
    end
    def get_readme_md
      @readme_md= get("https://bitbucket.org/#{@user}/#{@repo_name}/raw/#{@commit}/#{@readme_file}", json: false)
    end
  end
end
