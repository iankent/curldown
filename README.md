# Curldown

A simple tool to parse hosted markdown files into HTML.

## Usage

Either specify github credentials.

```ruby
Curldown.render(user: 'twbs', repo: 'bootstrap', github: true)
```

Or override with a URL.

```ruby
Curldown.render(url: 'https://raw.githubusercontent.com/twbs/bootstrap/master/README.md')
```

Code blocks are covered by [Pygments.rb](https://github.com/tmm1/pygments.rb) so if you want code highlighting make sure you include the appropriate css file.

```html
<link rel="stylesheet" href="https://raw.github.com/richleland/pygments-css/master/default.css" />
```

Or dynamically create one using the templating language of your choice.

```ruby
# using the slim templating language
style
	== Pygments.css
```

## Installation

```sh
gem install curldown
```

or include it in your gemfile
```ruby
gem "curldown", "~> 2.1"
```

or

```sh
git clone https://github.com/iankent/curldown.git
cd curldown
gem build curldown.gemspec
gem install ./curldown-v-v-v.gem
# where v-v-v is the version number
```

Enjoy!
