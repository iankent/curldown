Gem::Specification.new do |s|
	s.name = "curldown"
	s.version = "2.1.0"
	s.date = Time.now.strftime "%Y-%m-%d"
	s.summary = "Curldown"
	s.description = "A simple tool to parse hosted markdown files into html"
	s.author = "Ian Kent"
	s.email = "ian@iankent.me"
	s.files = Dir['lib/*.rb']
	s.homepage = "https://bitbucket.org/iankentforrestbrown/curldown"
	s.license = "MIT"
  s.add_dependency("curb", "~> 0.9")
  s.add_dependency("redcarpet", "~> 3.5")
	s.add_dependency("nokogiri", "~> 1.11")
	s.add_dependency("pygments.rb", "~> 2.0")
end
